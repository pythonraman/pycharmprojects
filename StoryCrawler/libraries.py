import re, os, requests, codecs, json
from bs4 import BeautifulSoup
from selenium import webdriver
from easygui import passwordbox, enterbox
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options

class WebDriver:
    def __init__(self):
        self.driver = webdriver.Chrome(executable_path="c:\\chromedriver.exe")

def loginToPage(url, driver):
    #driver.implicitly_wait(10)
    driver.get(url)
    print('Asking about priveliges.')
    driver.find_element_by_name("username").clear()
    driver.find_element_by_name("username").send_keys("username")
    driver.find_element_by_name("password").clear()
    password = passwordbox("PASSWORD:")
    driver.find_element_by_name("password").send_keys(password)
    driver.find_element_by_name("login").click()

def takeSoupfrom(url_main, urlc, driver):
    driver.implicitly_wait(10)
    #print(url+'0')
    driver.get(url_main+urlc+'0')
    source_code = driver.page_source
    soup = BeautifulSoup(source_code, "html.parser")
    soup = soup.find('div', {'class': 'pagination'})
    #print(soup.find_all('a', href=True))
    link_list = [a['href'] for a in soup.find_all('a', href=True)]
    print(link_list)
    #print(link_list[0].replace(urlc, ''))
    amount = int(link_list[0].replace(urlc, ''))
    counter = int(link_list[-2].replace(urlc, ''))
    return int(counter), amount


def save_page(soup, file):
    file = codecs.open(file, 'w+', 'utf-8')
    file.write(str(soup))
    file.close()
    return

def extract_data(counter, amount, url, driver):
    desc = []
    data = []
    ask = re.compile(r'if\(qu\(\)\) quoteAuthor = "(.*)"', re.I)
    for x in range(0, counter+1, amount):
        soup = source(url, str(x), driver)
        '''data += zip([a.string for a in soup.find_all('a', {'title': "Zobacz profil autora"})],
                    [a.find('span', {'class': "postbody"}).text for a in soup.find_all('td', {'onmouseup': True, 'valign': True})
                    if not re.search('<div style', a.find('span', {'class': "postbody"}).text)])'''
        #td = soup.find_all('td', {'onmouseup': True, 'valign': True})
        data += zip([re.search(ask, td['onmouseup']).group(1) for td in soup.find_all('td', {'onmouseup': True, 'valign': True})
                     if not td.find('div', {'style': True})],
                    [a.find('span', {'class': "postbody"}).text for a in
                     soup.find_all('td', {'onmouseup': True, 'valign': True})
                     if not re.search('<div style', a.find('span', {'class': "postbody"}).text)])
        #data = [a.name for soup in data for a in soup.find_all('a', {'title': "Zobacz profil autora"})]
        #desc += [a.find('span', {'class': "postbody"}).text for a in soup.find_all('td', {'onmouseup': True, 'valign': True})]
        #print(soup.find_all('td', {'onmouseup': True, 'valign': True}))
        #print('--{}: {}'.format(len(desc), desc))

    #print('##{}: {}'.format(len(data), data))
    return data

def convertToDict(data):
    dict = {}
    lista = []
    for record in data:
        v, k = record
        dict['Author'] = v
        dict['Description'] = k
        lista.append(dict)
    return lista

def writeFile(path, data, transfer):
    with codecs.open(path, "w", "UTF-8") as file:
        if transfer == 0:
            for lane in data:
                file.write('{}\n'.format(lane))
        if transfer == 2:
            for element in data:
                #if re.search('<div style', element[1]):
                #    continue
                file.write('Author: {}\n'.format(element[0]))
                file.write('Description:\n')
                for lane in element[1].splitlines():
                    if lane:
                        file.write('{}\n'.format(lane))
                file.write('\n')
        if transfer == 3:
            json.dump(data, file, indent=4)



def source(url, x, driver):
    driver.get(url+x)
    print(url+x)
    source_code = driver.page_source
    soup = BeautifulSoup(source_code, "html.parser")
    save_page(soup, '2soup{}.txt'.format(x))
    return soup

