import os, random, math
from tkinter import *
import total

samogloska='aeiouy'
spolgloska='bcdfghjklmnprstvwz'
litery=[samogloska, spolgloska]


def imie_def2():
    timie = ''
    zmienna = []
    for i in range(0, random.randint(4, 10)):
        zmienna.append(random.randint(0, 1))
        if len(zmienna)>2:
            if zmienna[-2] == zmienna[-3] == zmienna[-1]:
                zmienna [-1] = not zmienna[-1]
        x = zmienna[-1]
        timie += litery[x][random.randint(0, len(litery[x]) - 1)]
        timie = timie.capitalize()
    return timie

def imie_def():
    total.imie=imie_def2()
    ImieFrame = Label(frame, text="                       ")
    ImieFrame.grid(row=0)
    ImieFrame = Label(frame, text=total.imie)
    ImieFrame.grid(row=0)

def imie_save():
    if total.imie != ('' and 'Wygeneruj imię'):
        root.destroy()
    if total.imie == ('' and 'Wygeneruj imię'):
        ImieFrame = Label(frame, text='Wygeneruj imię')
        ImieFrame.grid(row=0)
    print(total.imie)
    return total.imie

def run():
    root = Tk()
    frame = Frame(root, width=300, height=200)

    ImieFrame = Label(frame, text="                        ")
    ImieFrame.grid(row=0)
    button1 = Button(frame, text="GENERATE", command=imie_def, padx=2, pady=2)
    button1.grid(row=1)
    button2 = Button(frame, text="OK", command=imie_save, padx=2, pady=2)
    button2.grid(row=1, column=1)

    frame.pack()
    root.mainloop()

if __name__== "__main__":
    run()

