import os, os.path
import codecs, re, hashlib, json
import errno
from bs4 import BeautifulSoup
from datetime import datetime

class HistoryLane:
    def __init__(self, date=None, username=None, field=None, change=None):
        self.date = date
        self.username = username
        self.field = field
        self.change = change
    def __repr__(self):
        return ('[{},{},{},{}]'.format(self.date, self.username, self.field, self.change))

'''class HistoryTicket(HistoryLane):
    def __init__(self):
        self.history = [HistoryLane.date, HistoryLane.username, HistoryLane.field, HistoryLane.change]
    def adding(self, data):
        self.history.append([data])
    def __repr__(self):
        return ('{}'.format(self.history))'''

class TicketJSONFile:
    #def __init__(self, id=0, category=None, date_sub=None, date_closed=None, reporter=None, summary=None, description=None, steps=None, history=[]):
    def __init__(self):
        self.id = '' #id
        self.category = '' #category
        self.date_sub = '' #date_sub
        self.date_closed = '' #date_closed
        self.reporter = '' #reporter
        self.summary = '' #summary
        self.description = '' #description
        self.steps = '' #steps
        self.history = '' #history
    def __repr__(self):
        return ('[ID:{}, Category:{}, Submited Date:{}, Closed Date:{}, Reporter:{}, Summary:{}, Description:{},'
               ' StepsToReproduce:{}, History:{}]'.format(self.id, self.category, self.date_sub, self.date_closed,
                                                           self.reporter, self.summary, self.description, self.steps, self.history))

def ticket_download(ticket, urlt, driver):
    ticket_url = urlt + ticket
    driver.get(ticket_url)
    source_code = driver.page_source
    soup = BeautifulSoup(source_code, "html.parser")
    soup = str(soup)
    root = os.getcwd()
    #print(root)
    ticket_file = os.path.join(root, 'Tickets', 'Ticket' + str(ticket) + '.txt')
    if os.path.isfile(ticket_file):
        # file = codecs.open(ticket_file, 'w', 'utf-8')
        print('Ticket exists, checking size...')
        #file_ticket_sum = ticket_checksum(ticket_file)
        file_ticket_size = os.path.getsize(ticket_file)
        #print('Ticket Size: <{}>'.format(file_ticket_size))
    else:
        file_size = 0
        print('Ticket don\'t exists in local database, downloading...')
        save_ticket(soup, ticket_file)
        return ticket_file
    temp_file = os.path.join(root, 'Tickets', 'temp.txt')
    file = codecs.open(temp_file, 'w', 'utf-8')
    file.write(soup)
    file.close()
    #file_temp_sum = ticket_checksum(temp_file)
    file_temp_size = os.path.getsize(temp_file)
    #print('Temp file Size: <{}>'.format(file_ticket_size))
    #if file_temp_sum == 0:
    #    print('COS NIE TAK Z SUMAMI KONTROLNYMI: temp_file_sum={}, ticket_file_sum={}'.format(file_temp_sum, file_ticket_sum))
    if file_temp_size == 0:
        print('COS NIE TAK Z SIZE OF FILES: temp_file_size={}, ticket_file_size={}'.format(file_temp_size, file_ticket_size))
    #print(file_ticket_size != file_temp_size, file_ticket_size, file_temp_size)
    if file_ticket_size != file_temp_size:
        print('Different sizes, saving new version.')
        save_ticket(soup, ticket_file)
    else:
        print('Sizes are equal.')
    return ticket_file



def ticket_checksum(filename):
    sha256_hash = hashlib.sha256()
    with open(filename, "rb") as f:
    #Read and update hash string value in blocks of 4K
        for byte_block in iter(lambda: f.read(4096), b""):
            sha256_hash.update(byte_block)
        print('{:80}:{}'.format(filename, sha256_hash.hexdigest()))
    return sha256_hash.hexdigest()


def save_ticket(soup, ticket_file):
    file = codecs.open(ticket_file, 'w+', 'utf-8')
    file.write(soup)
    file.close()
    return

def sort_rules():
    pass

def tickets_find(driver, url):
    #print(url)
    source_code = driver.page_source
    #print(source_code)
    soup = BeautifulSoup(source_code, "html.parser")
    #soup = soup.findAll('tr', {'bgcolor': re.compile('.*')})
    ask = re.compile(r'/app/mantis/view\.php\?id=\d*', re.I)
    soup = soup.find_all('a', href=ask, title=None)
    '''for each in soup:
        print(each)'''
    tickets_list = []
    for each in soup:
        each = each.string
        if re.search('\D', each):
            continue
        tickets_list.append(each)
    return tickets_list

def findTickets(startpath):
    list = []
    s = re.compile(r'Ticket\d{6,8}\.(txt)', re.I)
    for root, dirs, files in os.walk(startpath):
        for file in files:
            if re.match(s, file):
                list.append(os.path.join(root, file))
    return list

def fileToJSON(file):
    JSON = TicketJSONFile()
    soup = loadFile(file, parser=1)
    #print(soup)
    #Search history
    #print(searchData(soup)[1])
    history = searchData(soup)
    JSON.history = []
    for each in history:
        JSON.history.append(each.__dict__)
    #print(JSON.history.__dict__)
    # search ticket ID
    title = soup.find('title').text
    #print(title)
    #ask = re.search(r'((\d*):\s*(.*)_(.*)_(.*)_(\D{2})_(.*)_(\d{5,6})_(.*?))(\s*-\s*MantisBT)', title)
    ask = re.search(r'(\d{7,8}):', title)
    #print(ask)
    JSON.id = ask.group(1)
    #search category
    categories = soup.find_all('table')[3].find_all('td')
    JSON.category = categories[11].text
    JSON.date_sub = categories[13].text
    JSON.reporter = searchCategory(categories, 'Reporter')
    JSON.summary = searchCategory(categories, 'Summary')
    JSON.description = str(searchCategory(categories, 'Description of problem and consequences'))
    if len(JSON.description) != 0:
        #print('Y')
        JSON.description = JSON.description.splitlines()
    #print(json.dumps(JSON.description.splitlines(), indent=4))
    JSON.steps = str(searchCategory(categories, 'Steps To Reproduce'))
    #print(JSON.steps)
    if len(JSON.steps) != 0:
        JSON.steps = JSON.steps.splitlines()
        replace = []
        for each in JSON.steps:
            replace.append(each.replace(u'\xa0', ' '))
        JSON.steps = replace
    #print('{}'.format(JSON.steps))
    #for n in range(0, len(categories)):
    #    print('{}. {}'.format(n, categories[n]))
    #print(JSON.steps)
    return JSON

def searchCategory(categories, category_name):
    for n in range(0, len(categories)):
        #print(categories[n].text == category_name)
        if categories[n].text == category_name:
            return categories[n+1].text

def createJSONfiles():
    #list = findTickets(os.getcwd())
    list = loadFile(os.path.join(os.getcwd(), 'Tickets_list.txt'), parser=0)
    sorted_by = list.pop(0)
    #print(list)
    print('JSON files generator started.')
    #list = [r'C:\Users\PL1ijk0\PycharmProjects\ServiceChampion\Tickets\Ticket0217171.txt']
    for n, file in enumerate(list, 1):
        if any(x for x in [n % 50 == 0, n == len(list), n == 1]):
            print('Working... {}/{} file is generated. (Be patient)'.format(n, len(list)))
        JSON_data = fileToJSON(file)
        JSON_data = JSON_data.__dict__
        #print(JSON_data)
        root, path = os.path.split(file)
        root, dir = os.path.split(root)
        path, ext = os.path.splitext(path)
        JSON_file = os.path.join(root, 'JSON', path + '.json')
        writeFile(JSON_file, JSON_data, transfer=3)
        #print(JSON_file)
        #writeFile(JSON_file, JSON_data, transfer=2)
    print('Finished.')



def searchData(soup):
    #Search ticket history
    history = soup.find('div', id='history_open').find_all('td')
    #print(history)
    ticketHistory = []
    #ticketHistoryDict = []
    #print(len(history))
    for n in range(5, len(history), 4):
        lane = HistoryLane()
        lane.date = history[n].text.strip()
        lane.username = history[n + 1].text.strip()
        lane.field = history[n + 2].text.strip()
        lane.change = history[n + 3].text.strip().rstrip(',')
        #print(lane)
        #ticketHistoryDict.append(lane.__dict__)
        ticketHistory.append(lane)
    #print(ticketHistory[30].date)
    #print(ticketHistory)
    return ticketHistory#, ticketHistoryDict

def data(list):
    lista = []
    for n, each_path in enumerate(list, 1):
        soup = loadFile(each_path, parser=1)
        data = searchData(soup)
        #print(data)
        extractInfo(data, each_path, lista)
        if n % 50 == 0 or n == len(list) or n == 1:
            print('Working... {}/{} files done. (Be patient)'.format(n, len(list)))
        #break
        #writeFile(table, data)
    return lista

def extractInfo(data, file, lista):
    #print(team_1_st_level())
    for_1st_level = re.compile(r'=> 01_1st_Level')
    for_2nd_level = re.compile(r'\w{1,}\s*=> 02_2nd_Level')
    #n = 1
    for lane in data:
        if re.search(for_2nd_level, lane.change) and any([re.search(x, lane.username) for x in team_1_st_level()]):
            lista.append([file, lane.username])
            #print('Break on {} in {}'.format(n, file))
            break
        #n += 1
    return lista

def loadFile(file, parser):
    file_open = codecs.open(file, "r", "UTF-8")
    if parser == 1:
        soup = BeautifulSoup(file_open, "html.parser")
    if parser == 0:
        soup = file_open.read().splitlines()
    file_open.close()
    return soup

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def writeFile(path, data, transfer):
    file = codecs.open(path, "w", "UTF-8")
    if transfer == 1:
        for n, lane in enumerate(data):
            if n == 0 or n == 1:
                file.write('{}\n'.format(lane))
            else:
                file.write('Who transfered: {} in {}\n'.format(lane[1], re.search(r'Ticket\d{6,8}', lane[0]).group(0)))
    if transfer == 0:
        for lane in data:
            file.write('{}\n'.format(lane))
    if transfer == 2:
        file.write('{\n')
        for key, item in data.items():
            if key == 'description':
                item = item.splitlines()
                print(item)
                end = len(item)
                for n in range(0, end):
                    if n == 0:
                        file.write('{}: [{},\n'.format(json.dumps(key), json.dumps(item[n])))
                    if n == end-1:
                        file.write('{:16}{}],\n'.format('', json.dumps(item[n])))
                    if all(n != x for x in [0, end-1]):
                        file.write('{:16}{},\n'.format('', json.dumps(item[n])))
            #if key == 'description':
            #    file.write('{}: {},\n'.format(json.dumps(key), json.dumps(item.splitlines())))
            if key == 'history':
                end = len(item)
                for n in range(0, end):
                    if n == 0:
                        #print(item[n])
                        file.write('"{}": [{},\n'.format(key, json.dumps(item[n])))
                    if n == end-1:
                        file.write('{:12}{}]\n'.format('', json.dumps(item[n])))
                    else:
                        file.write('{:12}{},\n'.format('', json.dumps(item[n])))
            if all(key != x for x in ['description', 'history']):
                file.write('{}: {},\n'.format(json.dumps(key), json.dumps(item)))
        file.write('}')
    if transfer == 3:
        json.dump(data, file, indent=4)
    file.close()

def extract_data():
    print('Extracting data.')
    print('Started working...')
    #list = findTickets(os.getcwd())
    list = loadFile(os.path.join(os.getcwd(), 'Tickets_list.txt'), parser=0)
    sorted_by = list.pop(0)
    #print(list)
    tickets = data(list)
    #countUsernames(tickets)
    #print(tickets)
    howMany = ('I founded {}/{} ({:.1%}) tickets transfered from 1st_level to 2nd_level.'.format(len(tickets), len(list), len(tickets)/len(list)))
    print(howMany)
    tickets.insert(0, howMany)
    tickets.insert(0, sorted_by)
    writeFile(os.path.join(os.getcwd(), datetime.now().strftime("%Y%m%d_%H%M") + '_Ticket_1st_to_2nd_level.txt'), tickets, transfer=1)
    print('Finished.')

def countUsernames(lista):
    usernames = []
    for each in lista:
        usernames.append(each)
    pass

def team_1_st_level():
     return ['Nowak Filip', 'Klepuszewski Filip', 'Jaszczak Ireneusz', 'Faller Marcus', 'Maciag Rafal',
                       'Wozniak Wiktor', '01_1st_Level', 'Matylewski Konrad']

def search_date(start_data, end_data):
    #print({"start_year": start_data[2], "start_month": start_data[1], "start_day": start_data[0],
    #                "end_year": end_data[2], "end_month": end_data[1], "end_day": end_data[0]})
    return {"start_year": str(start_data[2]), "start_month": str(start_data[1]), "start_day": str(start_data[0]),
                    "end_year": str(end_data[2]), "end_month": str(end_data[1]), "end_day": str(end_data[0])}

def createFilterDate(filter):
    return '{} {} {} - {} {} {}'.format(filter['start_day'], filter['start_month'],
                                      filter['start_year'], filter['end_day'], filter['end_month'],
                                      filter['end_year'])




