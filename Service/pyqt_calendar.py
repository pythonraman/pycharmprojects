from PyQt5.QtWidgets import (QWidget, QCalendarWidget,
                             QLabel, QApplication, QVBoxLayout, QPushButton, QDialog)
from PyQt5.QtCore import QDate
from PyQt5.QtCore import pyqtSlot
import sys
import datetime, calendar


'''class ReturnDate:
    def __init__(self, choosen_date):
        super().__init__
    def __call__(self):
        #print('OK!')
        print('---------')
        print(choosen_date)
        self.close()
        return choosen_date'''

class Example(QDialog):

    def __init__(self, title):
        super().__init__()
        self.initUI(title)

    def initUI(self, title):
        vbox = QVBoxLayout(self)
        global choosen_date
        choosen_date = QDate.currentDate().toString("d,M,yyyy").split(',')
        choosen_date[1] = calendar.month_name[int(choosen_date[1])]
        print(choosen_date)
        cal = QCalendarWidget(self)
        cal.setMaximumDate(datetime.date.today())
        cal.setMinimumDate(QDate(2015, 1, 1))
        cal.setGridVisible(True)
        cal.clicked[QDate].connect(self.showDate)
        vbox.addWidget(cal)

        btn = QPushButton(self)
        btn.setText("OK")
        btn.clicked.connect(self.close)
        vbox.addWidget(btn)



        #self.lbl = QLabel(self)
        #date = cal.selectedDate()
        #self.lbl.setText(date.toString("dd-MM-yyyy"))

        #vbox.addWidget(self.lbl)

        self.setLayout(vbox)

        self.setGeometry(300, 300, 350, 300)
        self.setWindowTitle(title)
        self.show()

    def showDate(self, date):
        #self.lbl.setText(date.toString("dd-MM-yyyy"))
        global choosen_date
        choosen_date = date.toString("d,M,yyyy").split(',')
        #print(calendar.month_name[4])
        choosen_date[1] = calendar.month_name[int(choosen_date[1])]
        #print(choosen_date)
        return choosen_date

    #@pyqtSlot()
    @staticmethod
    def returnDate(ex):
        #print('OK!')
        #print('---------')
        #print(choosen_date)
        #self.close()
        ex.exec_()
        return choosen_date


#if __name__ == '__main__':
def choice(title):
    app = QApplication(sys.argv)
    #app.setQuitOnLastWindowClosed(False)
    ex = Example(title)
    return ex.returnDate(ex)
    #sys.exit(app.exec_())


#choice('Choose DATA')
