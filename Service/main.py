from libraries import *
from pyqt_calendar import *

from easygui import passwordbox, enterbox
#from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time


class WebFrame:
    def __init__(self):
        self.urlt = 'https://www.page.com/app/mantis/view.php?id='
        self.url = 'https://www.page.com'
        self.urlall = 'https://www.page.com/app/mantis/view_all_bug_page.php'
        self.urlsort = 'https://www.page.com/app/mantis/view_filters_page.php?for_screen=1&view_type=advanced&target_field=show_category[]'
    def select_dict_elements(self, name, elements, driver):
        driver.find_element_by_name(name).click()

        for key, value in elements.items():
            select = Select(driver.find_element_by_name(key))
            select.select_by_visible_text(value)
    def select_frame_elements(self, name, elements, driver):
        #print(elements)
        select = Select(driver.find_element_by_name(name))
        for element in elements:
            select.select_by_visible_text(element)
            select.deselect_by_visible_text('[any]')
            select.deselect_by_visible_text('Aloksej Vladimir')
    def start(self, url, urlall, urlsort):
        options = Options()
        options.add_argument('--headless')
        path = os.getcwd()+'\chromedriver.exe'
        driver = webdriver.Chrome(executable_path=path, options=options)
        #driver.set_window_size(1, 1)
        driver.implicitly_wait(20)
        driver.get(url)
        print('Asking about priveliges.')
        user = enterbox("USER:")
        driver.find_element_by_name("username").clear()
        driver.find_element_by_name("username").send_keys(user)
        driver.find_element_by_name("password").clear()
        password = passwordbox("Service Champion PASSWORD:")
        driver.find_element_by_name("password").send_keys(password)
        driver.find_element_by_id("login_button").click()
        driver.find_element_by_name("mtan").clear()
        mtan = enterbox("mtan:")
        driver.find_element_by_name("mtan").send_keys(mtan)
        driver.find_element_by_id("login_button").click()
        driver.get(urlsort)
        print('Setup filters...')
        driver.implicitly_wait(200)
        driver.find_element_by_name('show_category[]').click()
        driver.implicitly_wait(200)
        select = Select(driver.find_element_by_name('show_category[]'))
        select.select_by_visible_text('Support/Service')
        select.deselect_by_visible_text('General')
        #Setup users
        self.select_frame_elements("reporter_id[]", team_1_st_level(), driver)
        #Setup search period
        start_data = choice('Select start date')
        #print(start_data)
        end_data = choice('Select end date')
        #print(end_data)
        filter_data = search_date(start_data, end_data)
        #print(filter_data)
        print('The chosen filter data: {}'.format(createFilterDate(filter_data)))
        self.select_dict_elements("do_filter_by_date", filter_data, driver)
        #Setup how many pages
        driver.find_element_by_name("per_page").clear()
        driver.find_element_by_name("per_page").send_keys("1000000")
        #APPEND FILTERS
        driver.find_element_by_name("filter").click()
        driver.implicitly_wait(200)
        tickets_list = tickets_find(driver, driver.current_url)
        print('I found {} tickets corresponding filter restriction.'.format(len(tickets_list)))
        tickets_files_list = []
        for x, each_ticket in enumerate(tickets_list, 1):
            print('Nr {}/{}: Ticket #{}'.format(x, len(tickets_list), each_ticket))
            tickets_files_list.append(ticket_download(each_ticket, newframe.urlt, driver))
            #print(tickets_files_list)
        print('All DONE here.')
        #time.sleep(3000)
        return tickets_files_list, filter_data


mkdir_p(os.path.join(os.getcwd(), 'Tickets'))
mkdir_p(os.path.join(os.getcwd(), 'JSON'))
newframe = WebFrame()

list, filter_data = newframe.start(newframe.url, newframe.urlall, newframe.urlsort)
list.insert(0, 'Ticket(s) found with filter: {}.'.format(createFilterDate(filter_data)))
#print(list)
print('Saving list of tickets.')
writeFile(os.path.join(os.getcwd(), 'Tickets_list.txt'), list, transfer=0)
time.sleep(5)
createJSONfiles()
extract_data()
#sort_rules()
#print(list)
