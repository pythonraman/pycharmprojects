import sys
from PyQt5.QtWidgets import (QWidget, QCalendarWidget,
                             QLabel, QApplication, QVBoxLayout, QPushButton, QDialog, QGridLayout, QGridLayout, QLineEdit)
from PyQt5.QtCore import QDate
from PyQt5.QtCore import pyqtSlot


class Widget(QDialog):
    def __init__(self):
        super().__init__()

        grid = QGridLayout()
        grid.setSpacing(3)

        self.edit_first = QLineEdit()
        grid.addWidget(QLabel('Question 1'), 1, 0)
        grid.addWidget(self.edit_first, 1, 1)

        #   add layout for second widget
        self.edit_second = QLineEdit()
        grid.addWidget(QLabel('Question 2'), 2, 0)
        grid.addWidget(self.edit_second, 2, 1)

        apply_button = QPushButton('Apply', self)
        apply_button.clicked.connect(self.close)

        grid.addWidget(apply_button, 4, 3)
        self.setLayout(grid)
        self.setGeometry(300, 300, 350, 300)

    def return_strings(self):
        #   Return list of values. It need map with str (self.lineedit.text() will return QString)
        return map(str, [self.edit_first.text(), self.edit_second.text()])

    @staticmethod
    def get_data():
        dialog = Widget()
        dialog.exec_()
        return dialog.return_strings()


def main():
    app = QApplication([])
    window = Widget()
    [print(x) for x in window.get_data()]  # window is value from edit field


if __name__ == '__main__':
    main()