
def stringer(lista):
    for string in lista:
        if string == 'Anna':
            continue
        if not string:
            continue
        yield string

lista = ['Zuzanna', 'Anna', 'Olga', [], '', None, 'Daria']
lista2 = ['Zuzanna', 'Anna', 'Olga', 'Daria']
#Aby uzyć max() trzeba miec wartosci. None, '', [] wyrzuci Error
print(lista)
print(list(stringer(lista)))
#print(max(stringer(lista)))
#print(max(lista2))

iterable = enumerate(lista2)

n, first = next(iterable)
print(first)
for n, k in iterable:
    print(k)

print("------------")
change_for_iter = iter(lista2)
print(change_for_iter)
print("------------")
print(change_for_iter.__next__())
for k in change_for_iter:
    print(k)
