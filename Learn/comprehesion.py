def is_prime(candidate):
  divisibility = [
      candidate % n != 0
      for n in range(2, candidate)
  ]
  #print(divisibility)
  return all(divisibility)

#print(is_prime(233))

#for x in range(1, 100):
    #if is_prime(x):
    #    print(x)


'''lista = [
    x
    for x in range(1, 100)
    if is_prime(x)]
print(lista)'''

'''print([(x, y ** 2)
       for x in range(1, 20)
       for y in range(1, 20)])'''

'''print([(x+1, y ** 2)
       for x, y in list([range(1, 20), range(1, 20)])
       ])'''

print([[x, x] for x in list(range(1, 20))])
