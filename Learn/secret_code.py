from string import ascii_lowercase
import random

random.seed
print(random.randrange(1, 25))
i = random.randrange(1, 25)
table = {index+i: letter for index, letter in enumerate(ascii_lowercase)}
print(table)
print(len(table))
table2 = {letter: index+random.randrange(25) for index, letter in enumerate(ascii_lowercase)}
print(table2)
print(len(table2))
