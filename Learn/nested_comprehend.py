
lista = [[1, 2],
          [3, 4],
          [5, 6]]

matrix = [
    [row * 3 + incr for incr in range(1, 4)]
    for row in range(3)
]
print(matrix)

matrix2 = []
for row in range(3):
    k = []
    for incr in range(1, 4):
        k.append(incr + row * 3)
    matrix2.append(k)

print(matrix2)
