from PyQt5.QtWidgets import QApplication, QPushButton, QMainWindow, QDialog, QVBoxLayout, QCalendarWidget, QLabel
import sys
from PyQt5 import QtGui

class Window(QDialog):
    def __init__(self):
        super().__init__()

        self.title = "PyQt5 Calendar"
        self.left = 500
        self.top = 200
        self.width = 400
        self.height = 300
        self.iconName = "home.png"
        self.setWindowTitle(self.title)
        self.setWindowIcon(QtGui.QIcon(self.iconName))
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.Calendar()

        self.show()

    def Calendar(self):
        vbox = QVBoxLayout()
        self.calendar = QCalendarWidget()
        self.calendar.setGridVisible(True)
        self.calendar.selectionChanged.connect(self.onSelectionChanged)

        self.label = QLabel()
        self.label.setFont(QtGui.QFont("Sanserif", 15))
        self.label.setStyleSheet('color:green')

        #self.btn = QPushButton

        vbox.addWidget(self.label)
        vbox.addWidget(self.calendar)
        #vbox.addWidget(self.btn)

        self.setLayout(vbox)

    def onSelectionChanged(self):
        ca = self.calendar.selectedDate()
        #print(ca)
        self.label.setText(ca.toString("yyyy-MM-dd"))

App = QApplication(sys.argv)
window = Window()
sys.exit(App.exec())
