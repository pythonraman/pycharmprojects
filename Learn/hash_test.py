from libraries import *

def ticket_checksum(filename):
    sha256_hash = hashlib.sha256()
    with open(filename, "rb") as f:
    #Read and update hash string value in blocks of 4K
        for byte_block in iter(lambda: f.read(4096), b""):
            sha256_hash.update(byte_block)
        print('{:80}:{}'.format(filename, sha256_hash.hexdigest()))
    return sha256_hash.hexdigest()

root = os.getcwd()
temp_file = os.path.join(root, 'Tickets', 'Ticket.txt')

for n in range(1, 5):
    ticket_checksum(temp_file)