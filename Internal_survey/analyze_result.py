import re, os
from openpyxl import Workbook
from collections import OrderedDict

class Dane:
    def __init__(self):
        self.list = []
        self.question = []
        self.solution = []
        self.position = []
        self.table = []
        self.score_table = [{'Yes': 1, "I don't know": 0, 'No': -1},
                       {'Easy': 2, 'Managable': 1, "I don't know": 0, 'Hard': -1, 'Very Hard': -2},
                       {'Yes': 2, 'Partially': 1, "I don't know": 0, 'No': -2}, {'Yes': 1, '': 0}]
        #Weight of questions in rows
        self.weight_table = [5, 5, 3, 3, -3, -3, -3, -5, -5, -5, -4, -5, 5, -3, -5, 5, 1]

def sort_clean(list):
    #print(list)
    sorted_q = (OrderedDict((x, True) for x in list).keys())
    #print(sorted_q)
    list.clear()
    #x = 1
    for each in sorted_q:
        list.append(each)
        #print('{}. {}'.format(x, each))
        #x += 1
    return list

def operations(csv):
    dane_reg = re.compile(r'([1-9][0-9]?)\.\s*(.*?)\s*\[(.*)\]', re.I)  # ([2-9][0-9]?)\s*(.*)\s*\[(.*)\]
    remove_reg = re.compile(r'^1\.\s*(.*?)\s*\[(.*)\]', re.I)
    x = 0
    for each in csv:
        line = (each.split("\",\""))
        #print(line)
        survey.list.append(line)
        if x == 0:
            for a in range(len(line)):
                re_data = re.search(dane_reg, line[a])
                re_remove = re.search(remove_reg, line[a])
                if all([re_data, not re_remove]):
                    #print(re_data.group(2))
                    survey.question.append(re_data.group(2))
                    survey.solution.append(re_data.group(3))
        x += 1
    printout()
    elements(survey.list)
    #print(survey.list)
    return

def elements(list):
    len_row = len(list[0])
    #print(list[0][10])
    print('#######')
    len_list = len(list)
    print('Len list:{}, Len row:{}'.format(len_list, len_row))
    #print(list)
    for i in range(len(survey.solution)):
        #string = r'([1-9][0-9]?)\.\s*(.*?)\s*\[(' + survey.solution[i] + ')\]'
        #print(string)
        #dane_reg = re.compile(string, re.I)
        dane_reg = re.compile(r'([1-9][0-9]?)\.\s*(.*?)\s*\[(.*)\]', re.I)
        remove_reg = re.compile(r'^1\.\s*(.*?)\s*\[(.*)\]', re.I)
        for x in range(len_list):
            for y in range(len_row):
                re_data = re.search(dane_reg, list[x][y])
                re_remove = re.search(remove_reg, list[x][y])
                success = False
                if re_data:
                    #print(re_data)
                    if re_data.group(3) == survey.solution[i]:
                        success = True
                if all([re_data, not re_remove, success]):
                    #print('{}# {}, pos:({},{})'.format(survey.solution[i], list[x][y], x, y))
                    answers = []
                    for r in range(1, len(list)):
                        answers.append(list[r][y])
                    score_type = 0
                    if any(re_data.group(2) == v for v in ('Useable to connect to Machine', 'Useable to connect to Software','IT equipment involved', 'IT Administration', 'Hardware', 'Customer Hardware', 'Customer Actions needed', 'Customer crew involved', 'security', 'Customer security', 'External security')):
                        score_type = 1
                    if any(re_data.group(2) == v for v in ('Difficulty of configuration', 'Difficulty of implementation on Customer side')):
                        score_type = 2
                    if any(re_data.group(2) == v for v in ('Responsibility on side', 'Responsibility on Customer side', 'Responsibility on external company')):
                        score_type = 3
                    if any(re_data.group(2) == v for v in ('Personal choice',)):
                        score_type = 4
                    answers = score(answers, score_type)
                    where = survey.question.index(re_data.group(2))
                    answers *= survey.weight_table[where]
                    survey.table.append([survey.solution[i], re_data.group(2), score_type, answers])
                    pass
    #print(len(survey.table))
    x = 1
    #for each in survey.table:
    #    print(str(x) + '.', each)
    #    x += 1
    #    if x == 18:
    #        x = 1

def build_xls_table():
    #print(survey.question.index('Useable to connect to Machine'))
    #print(survey.table)
    wb = Workbook()
    ws = wb.active
    for each in survey.table:
        r = survey.question.index(each[1]) + 1
        c = survey.solution.index(each[0]) + 1
        v = "{0:.2f}".format(each[3])
        #print('{}# {}'.format(each[1], q))
        ws.cell(row=r + 1, column=2, value=each[1]) #question w columnie 2
        ws.cell(row=1, column=c+2, value=each[0]) #solution w columnach po 2
        ws.cell(row=r+1, column=c+2, value=v)
    x = 1
    for nr in ws.rows:
        if x > 1:
            ws.cell(row=x, column=1, value=x-1)  #numeracja w columnie 1
        x += 1

    wb.save('internal_survey_results.xlsx')


def average_calc(digit, score_type):
    count = 0
    sum = 0
    for d in digit:
        if d !=0:
           count += 1
           sum += d
    if count == 0:
        average = 0
    else:
        if score_type != 4:
            average = sum/count
        else:
            average = sum
    return average

def score(answers, score_type):
    #print(answers, score_type)
    digit = []
    for v in range(len(answers)):
        #print(each)
        digit.append(survey.score_table[score_type-1][answers[v]])
        pass
    average = average_calc(digit, score_type)
    return average

def printout():
    question = sort_clean(survey.question)
    solution = sort_clean(survey.solution)
    print('--------')
    #for q in range(len(question)):
    #    print(q+1, question[q])
    print(question)
    print(solution)
    #print(survey.position)
    print('--------')
    if len(survey.weight_table) != len(survey.question):
        print('DLUGOSCI TABLIC ROZNE!!!!!')
    else:
        print('DLUGOSCI TABLIC OK')


#MAIN
csv = open('Remote Control.csv', "r")
survey = Dane()
operations(csv)
build_xls_table()
csv.close()

