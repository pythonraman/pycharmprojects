import os, codecs, re

class ErrorCode:
    def __init__(self, number=0, desc=''):
        self.number = number
        self.desc = desc


#x = ErrorCode(4563, 'zlo')
#print(x.number, x.desc)
regex_error = re.compile(r'(\d{4}(:)?(\d{1,2})?)\s*-> (.*)')
result = open("result.txt", "r")
path = []
regex_path = re.compile(r'<(.*)>')
for lane in result:
    if regex_path.match(lane):
        path = regex_path.search(lane)[1]
        #print(path)
    if re.search(regex_error, lane):
        print('{} -> {} {}'.format(regex_error.search(lane)[1], regex_error.search(lane)[4], path))
result.close()