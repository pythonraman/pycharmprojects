import re

#Examples of errors:
lista = [
    ';	ERROR 5405:04 -> Vakuumband nicht bereit',
    ';7000:01 --> program run procedure assignment',
    ';	ERROR 5406:002 -> Programmablauf',
    '; FehlerNr.: 3439:001 => Berechnete ID ist grösser als 31',
    '; FehlerNr.: 3301 => UV-Trockner PU3 überhitzt',
    '; WarnungNr.: 1183 => Farbstationen ausgeschaltet',
    '; FehlerNr.: 5018:005 =>  Panel konnte nicht einloggen. Funktion Panel überprüfen.',
    ';	ERROR 5469:03	Schrittmotor Vakuumvorschub ist nicht bereit.',
    '; *            ge„ndert. Topspeed in UN_Loop143_5 langsamer ge-'
    '; *	durch die neuen ersetzt. '
]


error_indicator = re.compile(r'((\d{2,4})(:)(\d{0,3}))\s*(-|=)?-?(>|\s)\s*(.*)', re.I)
exclude = re.compile(r'\d{8}')
exclude2 = re.compile(r'\d+\.\d+\s*:')
[print(re.findall(error_indicator, x), x) for x in lista]
