from operator import attrgetter
#import os, codecs
from openpyxl import Workbook

class ErrorCode:
    def __init__(self, number=0, desc='', desc2='', er_files=[]):
        self.number = number
        self.desc = desc
        self.desc2 = desc2
        self.files = [er_files]
    def adding(self, er_files):
        #self.files += ', {}'.format(er_files)
        er_files = er_files[0]
        self.files.append(er_files)
        #self.files = self.files.split
    def __repr__(self):
        return('{} -> {} {} {}\n'.format(self.number, self.desc, self.desc2, self.files))

def error_matrix():
    error_codes = open("error_codes.txt", "r")
    error_array = []
    for lane in error_codes:
        lane = lane.strip().split(sep='#')
        error = ErrorCode(lane[0], lane[1], lane[2], lane[3])
        error_array.append(error)
    error_codes.close()
    #print(error_array[0])
    return error_array

def error_check(error_array):
    for error in error_array:
        #print(error[0])
        pass

def error_where(error_array):
    file = open("error_where.txt", "w+")
    file_errors = open("error_list.csv", "w+")
    wb = Workbook()
    ws = wb.active
    first_lane = 'Error number          Description            Where\n-----------------------------------------------------------\n'
    file.write(first_lane)
    row = ['Error', 'Description1', 'Description2']
    ws.append(row)
    for error in error_array:
        file.write('{} -> {}\n'.format(error.number, error.desc))
        file_errors.write('{}#{}#{}\n'.format(error.number, error.desc, error.desc2))
        row = [error.number, error.desc, error.desc2]
        ws.append(row)
        for x in range(len(error.files)):
            file.write(' {}. {}\n'.format(x+1, error.files[x]))
    wb.save('Error_codes.xlsx')
    file.close()
    file_errors.close()

def save(error_array):
    file = open("sorted.txt", "w+")
    first_lane = '{}{}'.format('Error number          Description            Where\n','-'*110+'\n')
    file.write(first_lane)
    for error in error_array:
        file.write('{} -> {} {}\n'.format(error.number, error.desc, error.files))
    file.close()


def error_code_link(error_array):
    linked = ErrorCode()
    linked_array = []
    x = -1
    for e in error_array:
        if x == -1:
            linked = e
            x = 0
            #print(linked.number)
            continue
        if (e.number != linked.number or e.desc != linked.desc):
            linked_array.append(linked)
            #print('{} {} {} {}'.format(linked.number, e.number, e.number != linked.number, e.desc != linked.desc))
            #print('[{}]\n[{}]'.format(e.desc, linked.desc))
            linked = e
            #x += 1
            continue
        if e.number == linked.number or e.desc == linked.desc:
            linked.adding(e.files)
            continue
    return linked_array


print("Starting where_are_errors.py")
error_array = error_matrix()
sorted_e = sorted(sorted(error_array, key=attrgetter('desc')), key=attrgetter('number'))
save(sorted_e)
linked_e = error_code_link(sorted_e)
#print(linked_e)
error_where(linked_e)