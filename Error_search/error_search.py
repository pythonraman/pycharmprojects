import os, re, codecs

path = r'.\files'

def list_files(startpath):
    list = []
    s = re.compile(r'[a-zA-Z0-9_]*_init\.(prg)', re.I) #[a-zA-Z0-9_]*\.(?!txt)\w{3} #(prg|src)
    s2 = re.compile(r'[a-zA-Z0-9_]*\.(prg)', re.I)
    for root, dirs, files in os.walk(startpath):
        found = 0
        for file in files:
            #print(file)
            if re.match(s, file): #root == path and
                list.append(os.path.join(root, file))
                found = 1
                continue
        if found == 0:
            for file in files:
                re_file = re.search(r'([a-zA-Z0-9_]*)\.prg', file)
                re_root = re.search(r'([a-zA-Z0-9_]*)\.prj', root)
                if re_file != None and re_root != None:
                    if re_file.group(1) == re_root.group(1):
                        list.append(os.path.join(root, file))
                        print("znalazlem: {}".format(root))
                        continue

    #print(list)
    return list

def search_error(list):
    result = codecs.open("result.txt", "w+", "utf-8")
    error_codes = codecs.open("error_codes.txt", "w+", "utf-8")
    errors_in_file = codecs.open("errors_in_file.txt", "w+", "utf-8")
    #regex_error = re.compile(r'(\d{4}(:)?(\d{1,2})?)\s*->\s*(.*)')
    #Examples of errors:
    #;	ERROR 5405:04 -> Vakuumband nicht bereit
    #;7000:01 --> program run procedure assignment
    #;	ERROR 5406:002 -> Programmablauf
    #; FehlerNr.: 3439:001 => Berechnete ID ist grösser als 31
    error_indicator = re.compile(r'((\d{2,4})(:)(\d{0,3}))\s*(-|=)?-?(>|\s)\s*(.*)', re.I)
    #((\d{2,4})(:)(\d{0,3}))\s*(-|=)-?>\s*(.*)[^/][^r]
    #error_indicator2 = re.compile(r'ERROR\s*((\d{2,4})(:)(\d{0,3}))\s*(.*)[^/][^r]', re.I)
    exclude = re.compile(r'\d{8}')
    exclude2 = re.compile(r'\d+\.\d+\s*:')
    for path in list:
        file = codecs.open(path, "r", "ANSI")
        to_check = file.readlines()
        file.close()
        errors_in_file.write('Errors in file?: {}\n'.format(path))
        #print(path)
        #print(to_check)
        e = 0
        answer = 'NO'
        for x in range(len(to_check)):
            search = re.findall(error_indicator, to_check[x])
            #search2 = re.findall(error_indicator2, to_check[x])
            #print(search2)
            #print(search.group(4))
            search_str = str(search)
            #print(search_str)
            desc2 = ''
            if exclude.search(to_check[x]) or exclude2.search(to_check[x]):
                #print(exclude.search(search_str), exclude2.search(search_str))
                continue
            if search:
                if e == 0:
                    answer = 'YES'
                    errors_in_file.write('{}\n'.format(answer))
                    e = 1
                result.write('---In file <{}>\n{}'.format(path, to_check[x]))
                score = error_indicator.search(to_check[x])
                desc = score[6].rstrip()
                #print(score[0])
                #print('1-{},2-{},3-{},4-{},5-{},6-{}'.format(score[1], score[2], score[3], score[4], score[5], score[6]))
                #print(score[3])
                if score[4] != None:
                    if len(score[4]) == 1:
                        twodigits = '0{}'.format(score[4])
                    else:
                        twodigits = score[4]
                else:
                    twodigits = ''
                error_number = '{}:{}'.format(score[2], twodigits)
                #print(error_number)
                if re.match(r'^;',to_check[x+1]) and not error_indicator.search(to_check[x+1]):
                    result.write('{}'.format(to_check[x + 1]))
                    desc2 = to_check[x + 1]
                    desc2 = desc2[1:len(desc2)]
                    #desc += '{}'.format(to_check[x + 1])
                    desc2 = desc2.rstrip().lstrip()
                    if exclude.search(desc2):
                        desc2 = ""
                    #print(to_check[x+1])
                if desc2 == None:
                    desc2 = ""
                error_codes.write('{}#{}#{}#{}\n'.format(error_number, desc, desc2, path))
                #errors_in_file.write('{}\n'.format(score[1]))
        if answer == 'NO':
            errors_in_file.write('{}\n'.format(answer))

    result.close()
    error_codes.close()
    errors_in_file.close()
    print('DONE.')




list = list_files(path)
search_error(list)
#os.system('where_are_errors.py')
#import where_are_errors