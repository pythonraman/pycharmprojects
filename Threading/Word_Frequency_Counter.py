import requests
from bs4 import BeautifulSoup
import operator

def start(url):
    word_list = []
    source_code = requests.get(url).text
    print(requests.get(url).url)
    soup = BeautifulSoup(source_code, "html.parser")
    file = open('soup.txt', 'w')
    file.write(str(soup))
    file.close()
    print('---------------')
    for post_text in soup.findAll('p'):
        content = post_text.text
        print(content)
        words = str(content).lower().split()
        for each_word in words:
            word_list.append(each_word)
    print('*************')
    print(word_list)
    print(str(len(word_list)) + ' words.')
    clean_up_list(word_list)

def clean_up_list(word_list):
    clean_word_list=[]
    for word in word_list:
        symbols = "!@#$%^&*()_<>?/;:\",.[]{}+'`~\|"
        symbols_mail = "!#$%^&*()_<>?/;:\",[]{}+'`~\|"
        symbols_www = "*()_<>?;:\",.[]{}+'`"
        mail = 9

        if word[0:4] == 'www.':
            for i in range(0,len(symbols_www)):
                if word[len(word)-1] == symbols_www[x]:
                    word = word[0:len(word)-1]
        else:
            for x in range(1, len(word) - 1):
                if word[x] == '@':
                    mail = 1
                    for i in range(0, len(symbols_mail)):
                        word = word.replace(symbols_mail[i], "")
                    break
            if mail != 1:
                for i in range(0, len(symbols)):
                    word = word.replace(symbols[i], "")

        if len(word) > 0:
            clean_word_list.append(word)
    print(clean_word_list)
    print(str(len(clean_word_list)) + ' words.')
    create_dictionary(clean_word_list)

def create_dictionary(clean_word_list):
    word_count = {}
    for word in clean_word_list:
        if word in word_count:
            word_count[word] += 1
        else:
            word_count[word] = 1
    for key, value in sorted(word_count.items(), key=operator.itemgetter(1)):
        print(key, value)
    print(word_count)

url='http://xirit.pl'

start(url)


