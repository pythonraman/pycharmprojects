import easygui, random
from PIL import Image
from datetime import datetime

def read_img(file):
    img = Image.open(file)
    img = img.convert("RGBA")
    datas = img.getdata()
    width, height = img.size
    #print(list(datas))
    return list(datas), width, height

def rc():
    random.seed()
    return random.randrange(0, 256, 1)

def create_noise(file, width, height):
    noiseData = [(rc(), rc(), rc(), 255) for _ in range(0, width*height)]
    return saveData(noiseData, file, mode=1)

def saveData(data, file, mode):
    #print(data)
    img = Image.new('RGBA', (width, height))
    img.putdata(data)
    if mode == 1:
        file_name = 'Noise_{}'.format(file)
    if mode == 2:
        file_name = 'Encrypted_{}'.format(file)
    if mode == 3:
        file_name = 'Decrypted_{}'.format(file)
    img.save(file_name, "PNG")
    return file_name

def screen(a, x):
    print('{}: {}'.format(x, a))

def create_encrypted(file, noise_file):
    orgData, org_width, org_height = read_img(file)
    noiseData, noise_width, noise_height = read_img(noise_file)
    #print('Original:  {}'.format(orgData))
    #print('Noise:     {}'.format(noiseData))
    encData = []
    if org_width == noise_width and org_height == noise_height:
        [encData.append((tuple(a + b for a, b in zip(orgData[x], noiseData[x]))))
         for x, y in enumerate(noiseData)]
    else:
        print('Something wrong with sizes, they are not equal!\n')
        print('File: {} Width: {}, Height: {}\n'.format(file, org_width, org_height))
        print('File: {} Width: {}, Height: {}\n'.format(file, noise_width, noise_height))
    macierz = encData
    #print(macierz)
    encData = []
    for each in macierz:
        r, g, b, a = each
        a = 255
        for x, color in enumerate([r, g, b]):
            #check = None
            #print(x)
            if x == 0:
                r, check = enc_color_check(color)
                if check:
                    a -= 1
                    #screen(a, 'r')
            if x == 1:
                g, check = enc_color_check(color)
                if check:
                    a -= 2
                    #screen(a, 'g')
            if x == 2:
                b, check = enc_color_check(color)
                if check:
                    a -= 4
                    #screen(a, 'b')
        #print(a)
        encData.append(tuple([r, g, b, a]))
    #print('Encoded:   {}'.format(encData))
    return saveData(encData, file, mode=2)

def enc_color_check(color):
    if color > 255:
        color -= 255
        check = 1
    else:
        check = None
    return color, check


def decrypted(enc_file, noise_file):
    encData, enc_width, enc_height = read_img(enc_file)
    noiseData, noise_width, noise_height = read_img(noise_file)
    #print('---')
    #print(encData)
    decData = []
    if enc_width == noise_width and enc_height == noise_height:
        [decData.append((tuple(a - b for a, b in zip(encData[x], noiseData[x])))) for x, y in enumerate(noiseData)]
    else:
        print('Something wrong with sizes, they are not equal!\n')
        print('File: {} Width: {}, Height: {}\n'.format(file, org_width, org_height))
        print('File: {} Width: {}, Height: {}\n'.format(file, noise_width, noise_height))
    macierz = decData
    #print(macierz)
    decData = []
    for each in macierz:
        r, g, b, a = each
        go = check_ref(-a)
        #print(go)
        for x, color in enumerate([r, g, b]):
            if x == 0 and go[3] == 1:
                r = dec_color_check(color, a)
            if x == 1 and go[2] == 1:
                g = dec_color_check(color, a)
            if x == 2 and go[1] == 1:
                b = dec_color_check(color, a)
        decData.append(tuple([r, g, b, 255]))
    #print('Decrypted: {}'.format(decData))
    return saveData(decData, file, mode=3)

def dec_color_check(color, a):
    if color <= 0 and a != 255:
        color += 255
    #if color > 0 and a == 255:
    #    color = 0
    return color

def check_ref(a):
    binary = [0, 0, 0, 0]
    result = [int(x) for x in bin(a)[2:]]
    A, B = sorted([binary, result], key=len)
    for i, x in enumerate(reversed(A), 1):
        B[-i] += x
    return B

#opacity_level = 170 # Opaque is 255, input between 0-255
file = 'a6w2nl.jpg'
print('Read image...')
time = datetime.now()
datas, width, height = read_img(file)
time = datetime.now() - time
#print(time.strftime("%M:%S"))
#print('{}'.format(time.total_seconds()))
totalMinute, second = divmod(time.seconds, 60)
hour, minute = divmod(totalMinute, 60)
micros = time.microseconds
print(f"{hour}h {minute:02}m {second:02}s {micros}microseconds")
print('Done.')
#print('{}\n'.format([x for x in datas]), len(datas), width, height)
print('Creating filter...')
noise_file = create_noise(file, width, height)
print('Done.')
#noise_file = 'Noise_obraz.png'
print('Starting encoding...')
enc_file = create_encrypted(file, noise_file)
print('Done.')
print('Starting decoding...')
dec_file = decrypted(enc_file, noise_file)
print('Done.')



