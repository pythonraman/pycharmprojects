import os
import re
import time
import logging
from openpyxl import Workbook, load_workbook

formatters = {
    'RED': '\033[91m',
    'GREEN': '\033[92m',
    'END': '\033[0m',
}

def openWorkbook(file):
    start = time.time()
    wb = load_workbook(filename=file, read_only=True)
    logging.info(file)
    # print(wb.sheetnames)
    sheetnames = wb.sheetnames

    invoice = []
    data = []
    inumer = re.search(r'(\d{5,8})(\.xlsx)$', file).group(1)

    # iterate over all cells
    # iterate over all rows
    pcs = re.compile(r'pcs', re.I)
    dnote = re.compile(r'delivery\s*note', re.I)
    pnumber = re.compile(r'part\s*number', re.I)
    desc = re.compile(r'description', re.I)
    for sheetname in sheetnames:
        sheet_time = data_time = time.time()
        counting = 0
        desc_row = desc_col = dnote_col = pcs_col = None
        pnumber_col = 1
        ws = wb[sheetname]
        # get max row count
        max_row = ws.max_row
        # get max column count
        max_column = ws.max_column
        for i in range(1, max_row + 1):
            if re.search(pnumber, str(ws.cell(row=i, column=1).value)) and desc_row == None:
                desc_row = i
                logging.info('Found description in lane {} in sheet: "{}"'.format(desc_row, sheetname, file,
                                                                           time.time() - sheet_time, **formatters))
            if desc_row == None and i == 30:
                logging.info('BREAK sheet: "{}", no description'.format(sheetname, file))
                break
            if ws.cell(row=i, column=1).value == None and desc_row != None:
                # print(i)
                break
            # iterate over all columns
            if desc_row != None and pcs_col == None:
                for j in range(1, max_column + 1):
                    # get particular cell value
                    cell_obj = ws.cell(row=i, column=j)
                    if re.search(dnote, str(cell_obj.value)):
                        dnote_col = j
                    if re.search(pcs, str(cell_obj.value)):
                        pcs_col = j
                    # if re.search(pnumber, str(cell_obj.value)):
                    #    pnumber_col = j
                # print(pnumber_col, pcs_col, dnote_col)
                try:
                    logging.info('Finished searching columns in: {:.2f}s'.format(time.time() - sheet_time))
                except:
                    logging.info('Finished searching columns in: 0s')
                continue
            if desc_row != None:
                ''''# print new line
                print('\n')'''
                if counting == 0:
                    data_time = time.time()
                    counting = 1
                # data.update({'pnumber': ws.cell(row=i, column=pnumber_col).value, 'pcs': ws.cell(row=i,
                # column=pcs_col).value, 'dnote': ws.cell(row=i, column=dnote_col).value})
                if not re.search('-', str(ws.cell(row=i, column=pnumber_col).value)):
                    continue
                # print('<{}>'.format(ws.cell(row=i, column=dnote_col).value))
                if dnote_col != None and ws.cell(row=i, column=dnote_col).value != None:
                    invoice.append(
                        [inumer, ws.cell(row=i, column=pnumber_col).value, ws.cell(row=i, column=pcs_col).value,
                         ws.cell(row=i, column=dnote_col).value])
                else:
                    invoice.append(
                        [inumer, ws.cell(row=i, column=pnumber_col).value, ws.cell(row=i, column=pcs_col).value,
                         'N/A'])
        logging.info('Finished searching data in sheet: "{}" in: {:.2f}s'.format(sheetname, time.time() - data_time))

    end = time.time()
    timer = end - start
    logging.info('Time elapsed: {:.2f}s'.format(timer))
    return (invoice, timer)


def saveWorkbook(list):
    wb = Workbook(write_only=True)
    ws = wb.create_sheet()
    ws.append(['Invoice', 'Part number', 'Pcs', 'Delivery Note'])
    # now we'll fill it with 100 rows x 200 columns
    for file in list:
        for irow in file:
            # print(irow)
            ws.append(irow)
    # save the file
    wb.save('invoices.xlsx')


def findExcels(startpath):
    list = []
    s = re.compile(r'^[^~].*\d{6,8}\.xlsx$', re.I)
    arch = re.compile(r'archi', re.I)
    logging.info('START!')
    logging.info('Looking for Invoice Excels...')
    for root, dirs, files in os.walk(startpath):
        for file in files:
            # print(root)
            if re.search(arch, root):
                continue
            if re.search(s, file):
                list.append(os.path.join(root, file))
    logging.info('Found: {} Invoice Excels'.format(len(list), **formatters))
    return list


def createShipmentList(list):
    # invoices = {}
    invoices = []
    wholetime = 0
    for n, file in enumerate(list):
        # invoices.update(openWorkbook(file))
        if n % 1 == 0 or n == len(list) or n == 1:
            logging.info('Working... {}/{} files in progress. (Be patient)'.format(n+1, len(list), **formatters))
        # print(file)
        invoice, time = openWorkbook(file)
        invoices.append(invoice)
        wholetime += time
    return invoices, wholetime

#main
logging.basicConfig(filename='shipment.log', level=logging.INFO, format='%(asctime)s:%(levelname)s:%(message)s', filemode='w')
console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

#list = findExcels('c:\Specification of Custom Invoice')
list = findExcels(os.getcwd())
# print(list)
save, wholetime = createShipmentList(list)
saveWorkbook(save)
logging.info('Whole time of working... {:.2f}s.'.format(wholetime))
logging.info('DONE.')

