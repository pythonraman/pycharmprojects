from decimal import Decimal
# BINARY AND
a = 50      #110010
b = 25      #011001

c = a & b   #010000
print(c)

# BINARY RIGHT SHIFT
x = 240         #1110000
y = x >> 2      #0011100

print(y)

x = bin(y << 2)
print(x)
print(Decimal(x.strip()))


