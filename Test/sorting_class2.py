from operator import attrgetter

class User:
    def __init__(self, x):
        self.name = x[0]
        self.user_id = x[1]
    def __repr__(self):
        return(self.name + ' : ' + str(self.user_id))


users = [
    ('Bucky', 43),
    ('Tesla', 12),
    ('Adam', 1),
    ('Neal', 100)
]

user_list = []
for user in users:
    user_list.append(User(user))


for user in user_list:
    print(user)

print(user_list[2])

