import heapq

grades = [0, 10, 345, 200, 100, 400, 600]

#print(heapq.nlargest(3, grades))

stocks = [
    {'ticker': 'AAPL', 'price': 23},
    {'ticker': 'DOM', 'price': 235},
    {'ticker': 'TUNA', 'price': 34.3},
    {'ticker': 'TREE', 'price': 12}
]


print(heapq.nsmallest(2, stocks, key=lambda stock: stock['price']))
print('________')

for i in range(len(stocks)):
    print(stocks[i]['price'])
print('________')
#print(stocks['price'])

#for i in range(len(stocks))
#    price = stocks
#print((lambda x: x*2)(3))