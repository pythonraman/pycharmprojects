from collections import Counter

text = "Die Aspekte, die bei Integrationen zu beachten sind, sind in der Integrationanforderungscheckliste dokumentiert: :Media:D-800-117000-en_V1.1_redcube_and_redcube_plus_integration_requirements.pdf. Fragen zur Integration werden unter FAQ Machine integration beantwortet." \
"Weitere Informationen können im Handbuch nachgelesen werden: http://rothorn.hapa.ch/packages/RCControl/Manual/" \
"Vorbehandlung" \
"Möglichkeiten wie thermisch (Infrarot, Heissluft, Beflammung), elektrostatisch (Erdung, Ionizer), Reinigung (IPA, Wasser), aerodynamisch (Air knife, Absaugung), Beschichtung (Spray, Dip, Vordruck, Laminierung), Plasma (Corona, Plasma)" \
"        Spezialanwendung: Nur mit Redcube 36SL vertikal in Verbindung mit V3-Versorgungseinheit (Siehe Recube-Konfigurationen)" \
"Freigaben der einzelnen Redcube-Typen sind zu beachten" \
"Farbe / Weiss-Druck" \
"Nur mit Version 1.0 oder Version 3 möglich in Kombination mit passendem Druckkopf (Siehe Recube-Konfigurationen) "

words = text.split()
print(words)
print('_____________________')
counter = Counter(words)
print(counter)
top_three = counter.most_common(3)
print('_____________________')
print(top_three)