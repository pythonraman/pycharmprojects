#library.py

class BaseMeta(type):
    def __new__ (cls, name, bases, body):
        if not 'bar' in body:
            raise TypeError("Bad user class")
        print('BaseMeta.__new__', cls, name, bases, body)
        return super().__new__(cls, name, bases, body)

class Base(metaclass=BaseMeta):
    def foo(self):
        return self.bar()

'''old_bc = __build_class__
#def my_bc(*a, **kw):
#    print('my build class -->', a, kw)
#    return old_bc(*a, **kw)

def my_bc(fun, name, base=None, **kw):
    if base is Base:
        print('check if bar method defined')
    if base is not None:
        return old_bc(fun, name, base, **kw)
    return old_bc(fun, name, **kw)

import builtins
builtins.__build_class__ = my_bc'''

