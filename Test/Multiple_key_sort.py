from operator import itemgetter
import pprint

stocks = [
    {'ticker': 'AAPL', 'price': 'VVVVV'},
    {'ticker': 'DOM', 'price': 'BBBBB'},
    {'ticker': 'TUNA', 'price': 'XXXXX'},
    {'ticker': 'TREE', 'price': 'AAAAA'},
    {'ticker': 'TREE', 'price': 'XXXXX'},
    {'ticker': 'TREE', 'price': 'BBBBB'},
]

for x in sorted(stocks, key=itemgetter('ticker')):
    print(x)
print('---------')
for x in sorted(stocks, key=itemgetter('ticker', 'price')):
    print(x)
print('---------')
pprint.pprint(stocks)