import os, random, math
from tkinter import *


samogloska='aeiouy'
spolgloska='bcdfghjklmnprstvwz'
litery=[samogloska, spolgloska]

def imie_def():
    imie=''
    zmienna = []
    for i in range(0, random.randint(4, 10)):
        zmienna.append(random.randint(0, 1))
        if len(zmienna)>2:
            if zmienna[-2] == zmienna[-3] == zmienna[-1]:
                zmienna [-1] = not zmienna[-1]
        x = zmienna[-1]
        imie += litery[x][random.randint(0, len(litery[x]) - 1)]
    imie = imie.capitalize()
    ImieFrame = Label(frame, text="                       ")
    ImieFrame.grid(row=0)
    ImieFrame = Label(frame, text=imie)
    ImieFrame.grid(row=0)

root = Tk()
frame = Frame(root, width=300, height=200)

ImieFrame = Label(frame, text="                       ")
ImieFrame.grid(row=0)
button1 = Button(frame, text="GENERATE", command=imie_def, padx=2, pady=2)
button1.grid(row=1)

frame.pack()
root.mainloop()

