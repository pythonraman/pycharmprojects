import numpy
from colorama import Fore, Style

def printTable(table):
    table, n, k = checkElements(table)
    table = rotateTable(table)
    columns, max_list = checkTable(table)
    print(f'{Fore.BLUE}Nasza tabelka po uporządkowaniu i odwróceniu odbicia o 90 stopni:{Style.RESET_ALL}')
    for y in range(len(table[0])):
        for x in range(len(table)):
            table[x][y] = table[x][y].ljust(max_list[y],' ')
    for y in range(columns):
        row = '|'
        for x in range(len(table[0])):
            row += str(table[y][x]) + '|'
        if y == 0:
            print(len(row) * f'{Fore.RED}-{Style.RESET_ALL}')
        print(row)
        print(len(row) * f'{Fore.RED}-{Style.RESET_ALL}')

def print_row(row):
    for x in range(len(row)):
        print(row[x])


def rotateTable(table):
    tableRotate = [['' for x in range(len(table))] for y in range(len(table[0]))]
    for y in range(len(table[0])):
        for x in range(len(table)):
            tableRotate[y][x] = table[x][y]
    table = tableRotate
    return table

def checkElements(table):
    columns = len(table)
    ilosc_elementow = 0
    for x in range(columns):
        if len(table[x]) > ilosc_elementow:
            ilosc_elementow = len(table[x])
    for x in range(columns):
        table[x] += ['']*(ilosc_elementow - len(table[x]))
    return table, ilosc_elementow, columns

def checkTable(table):
    table, ilosc_elementow, columns = checkElements(table)
    elementLength = numpy.ones((columns, ilosc_elementow))
    max_list = ilosc_elementow*[None]
    for x in range(columns):
        for i in range(len(table[x])):
            elementLength[x][i] = len(table[x][i])
    for i in range(ilosc_elementow):
        maximum = 0
        for x in range(columns):
            if elementLength[x][i] > maximum:
                maximum = elementLength[x][i]
        max_list[i] = int(maximum)
    return columns, max_list

#START
tableData = [['jabłka', 'pomarańcze', 'banany', 'cytryny'], #
             ['Alicja', 'Bob', 'Karol', 'Światowid'],
             ['psy', 'koty', 'łososie', 'ryby'],
             ['1', '2', '3', '']]

print(f'{Fore.BLUE}Początkowa tabelka:{Style.RESET_ALL}')
for x in tableData:
    print(f'{Fore.YELLOW}' + str(x) + f'{Style.RESET_ALL}')
print('')
printTable(tableData)
