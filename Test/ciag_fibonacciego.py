import math

def kolejny_wyraz_ciagu(pwc1, pwc2):
    return ciag_fibonnaciego[pwc1]+ciag_fibonnaciego[pwc2]

def tworz_ciag(ciag_max):
    for i in range(ciag_max):
        ciag_fibonnaciego.append(kolejny_wyraz_ciagu(i,i+1))
    print(ciag_fibonnaciego)

def tworz_dzielnik(ciag):
    i = len(ciag)-1
    while i > 2:
        dzielnik.append(ciag[i]/ciag[i-1])
        i -= 1
    print(dzielnik)

def tworz_dzielnik_ostatni(ostatni, przedostatni):
    dzielnik_ostatni = ostatni/przedostatni
    print(dzielnik_ostatni)

ciag_fibonnaciego = [1, 1]
dzielnik = []

tworz_ciag(100)
#tworz_dzielnik(ciag_fibonnaciego)
tworz_dzielnik_ostatni(ciag_fibonnaciego[len(ciag_fibonnaciego)-1], ciag_fibonnaciego[len(ciag_fibonnaciego)-2])
#print(len(ciag_fibonnaciego))


