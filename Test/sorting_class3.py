from operator import attrgetter

class User:
    def __init__(self, x, y):
        self.name = x
        self.user_id = y
    def __repr__(self):
        return(self.name + ' : ' + str(self.user_id))


users = [
    User('Bucky', 43),
    User('Tesla', 12),
    User('Adam', 1),
    User('Neal', 100)
]

user = User('Bucky', 43)
print(users[0].user_id)