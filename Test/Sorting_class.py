from operator import attrgetter

class User:
    def __init__(self, x, y):
        self.name = x
        self.user_id = y
    def __repr__(self):
        return(self.name + ' : ' + str(self.user_id))


users = [
    ('Bucky', 43),
    ('Tesla', 12),
    ('Adam', 1),
    ('Neal', 100)
]


for user in users:
    print(User(user[0],user[1]))


users = [
    User('Bucky', 43),
    User('Tesla', 12),
    User('Adam', 1),
    User('Neal', 100)
]

print('------')
for user in users:
    print(user)
print('###############')
for user in sorted(users, key=attrgetter('name')):
    print(user)
print('^^^^^^^')
users = [
    ['Bucky', 43],
    ['Tesla', 12],
    ['Adam', 1],
    ['Neal', 100]
]

for user in users:
    #print(str(users.index(user)))
    print(str(users.index(user)) + '_ ' + str(User(user[0],user[1])))
#for user in sorted(users, key=attrgetter(users[user[0]])):
#    print(User(user[0], user[1]))
print(attrgetter(users[0][0]))