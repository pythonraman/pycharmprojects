import pyperclip, re

phoneRegex = re.compile(r'''(
    (\d{3}|\(\d{3}\))? #kierunkowy
    (\s|-|\.|\#)?
    (\d{3})
    (\s|-|\.|\#)?
    (\d{3})
    (\s*(ext|x|ext.)\s*(\d{2,5}))?  #wewnetrzny
    )''', re.VERBOSE | re.I)

emailRegex = re.compile(r'''(
    [a-zA-Z0-9._%+-]+  #nazwa uzytkownika
    @
    [a-zA-Z0-9._-]+    #nazwa domeny
    (\.[[a-zA-Z]{2,4}) #kropka plus extenstion
    )''', re.VERBOSE | re.I)


text = str(pyperclip.paste())
matches = []
try:
    for groups in phoneRegex.findall(text):
        phoneNum = '-'.join([groups[1], groups[3], groups[5]])
        if groups[8] != '':
            phoneNum += 'x' + groups[8]
        matches.append(phoneNum)
except:
    pass

try:
    for groups in emailRegex.findall(text):
        matches.append(groups[0])
except:
    pass

print(matches)