import codecs, re
from datetime import datetime

def loadFile(file, parser):
    file_open = codecs.open(file, "r", "UTF-8")
    if parser == 1:
        soup = BeautifulSoup(file_open, "html.parser")
    if parser == 0:
        soup = file_open.read().splitlines()
    file_open.close()
    return soup

def saveFile(data):
    file = codecs.open('downtime.csv', 'w+', 'utf-8')
    file.write('{}\n'.format('Date;End of Alarm; Start of Alarm;Time of Alarm'))
    for lane in data:
        file.write('{}\n'.format(lane))
    file.close()
    return

"""def unnecesaryNotification(output):
    for lane in output:
        print(lane)
    #print(output[0])
    return output_clear"""


if __name__ == '__main__':
    output = loadFile('tabela.csv', 0)
    #print(output)
    #output = unnecesaryNotification(output)
    tabela = []
    for lane in output:
        lane = lane.split(';')
        #print(lane)
        """if lane[1] == 'NOTIFICATION' and re.search(r'HTTP OK: Status line output matched', lane[3]):
            #print('match')
            continue
        else:
            #print(lane)"""
        tabela.append(lane)
    tabela.remove(tabela[0])
    #print(tabela[0][1])
    tabela_ok = []
    for p in range(0, len(tabela)):
        #date = tabela[p][0]
        status = tabela[p][1]
        #time = tabela[p][2]
        if status == 'OK':
            tabela_ok.append(p)
        #print(date, status, time)
    #print(tabela_ok)
    downtime = []
    for p in range(0, len(tabela_ok)-1):
        time_end = tabela[tabela_ok[p]][2]
        time_start = tabela[tabela_ok[p+1]-1][2]
        FMT = "%H:%M:%S"
        #print(time_end)
        #print(datetime.strptime(time_end, FMT).time())
        tdelta = datetime.strptime(time_end, FMT) - datetime.strptime(time_start, FMT)
        tdelta = str(tdelta)
        #tdelta = datetime.strftime(tdelta, FMT)
        if re.search(r'-1 day, ', tdelta):
            print('replace')
            tdelta = tdelta.replace('-1 day, ', '0')
        print(tdelta)
        downtime.append(tabela[tabela_ok[p]][0]+';'+time_end+';'+time_start+';'+str(tdelta))
        #print(time_end, time_start, tdelta)
    saveFile(downtime)

