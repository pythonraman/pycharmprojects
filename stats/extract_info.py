from selenium.webdriver.support.ui import Select
from easygui import passwordbox, enterbox
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import os, os.path, time
from bs4 import BeautifulSoup
import codecs

class WebFrame:
    def __init__(self):
        self.url_icinga = 'http://adres.com/icingaweb2/dashboard'
        self.url = 'http://adres.com/icingaweb2/monitoring/service/history?host=server.com&service=disk&page=14#page-14'
    """        
    def select_dict_elements(self, name, elements, driver):
        driver.find_element_by_name(name).click()
        #print('#'+str(elements))
        for key, value in elements.items():
            select = Select(driver.find_element_by_name(key))
            select.select_by_visible_text(value)
    def select_frame_elements(self, name, elements, driver):
        #print(elements)
        select = Select(driver.find_element_by_name(name))
        for element in elements:
            select.select_by_visible_text(element)
            select.deselect_by_visible_text('[any]')
            select.deselect_by_visible_text('')
    """
    def start(self, url_icinga, url):
        options = Options()
        #options.add_argument('--headless')
        options.add_argument("user-data-dir=selenium")
        path = os.getcwd()+'\chromedriver.exe'
        driver = webdriver.Chrome(executable_path=path, options=options)
        #driver.set_window_size(1, 1)
        driver.implicitly_wait(20)
        driver.get(url)
        print('Asking about priveliges.')
        #user = enterbox("USER:")
        driver.find_element_by_name("username").clear()
        driver.find_element_by_name("username").send_keys('username')
        driver.find_element_by_name("password").clear()
        password = passwordbox("Icinga PASSWORD:")
        driver.find_element_by_name("password").send_keys(password)
        driver.find_element_by_name("btn_submit").click()
        #driver.implicitly_wait(2000)
        #time.sleep(20)
        #driver.get(url)
        #driver.implicitly_wait(2000)
        time.sleep(5)
        source_code = driver.page_source
        # print(source_code)
        soup = BeautifulSoup(source_code, "html.parser")
        return soup

def saveText(text):
    file = codecs.open('text.txt', 'w+', 'utf-8')
    for lane in text:
        file.write('{}\n'.format(lane))
    #file.write(text)
    file.close()
    return

if __name__ == '__main__':
    newframe = WebFrame()
    soup = newframe.start(newframe.url_icinga, newframe.url)
    content = soup.find("div", {"class": "content"})
    text = content.find_all(text=True)
    output = []
    for t in text:
        if t == 'Load More':
            continue
        t = t.strip()
        t = t.lstrip('[').lstrip('icingaadmin').lstrip(']')
        if t:
            # output += '{}\n '.format(t)
            output.append(t)
    saveText(output)
