import codecs, re

def loadFile(file, parser):
    file_open = codecs.open(file, "r", "UTF-8")
    if parser == 1:
        soup = BeautifulSoup(file_open, "html.parser")
    if parser == 0:
        soup = file_open.read().splitlines()
    file_open.close()
    return soup

if __name__ == '__main__':
