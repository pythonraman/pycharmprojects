import codecs, re
from bs4 import BeautifulSoup


def loadFile(file, parser):
    file_open = codecs.open(file, "r", "UTF-8")
    if parser == 1:
        soup = BeautifulSoup(file_open, "html.parser")
    if parser == 0:
        soup = file_open.read().splitlines()
    file_open.close()
    return soup

def extractData(soup):
    content = soup.find_all("div", {"class": "content"})
    label = soup.find_all("div", {"class": "state-label"})
    date = soup.find_all("th", {"colspan": "2"})
    text = soup.find_all(text=True)
    output = []
    blacklist = [
        '[document]',
        'noscript',
        'header',
        'html',
        'meta',
        'head',
        'input',
        'script',
        'iframe',
        'title'
        # there may be more elements you don't want, such as "style", etc.
    ]
    for t in text:
        if t.parent.name not in blacklist:
            t = t.strip()
            t = t.lstrip('[').lstrip('icingaadmin').lstrip(']')
            if t:
                #output += '{}\n '.format(t)
                output.append(t)
    #print(output)
    return content, label, date, text, output



def saveFile(data):
    file = codecs.open('tabela.csv', 'w+', 'utf-8')
    file.write('{}\n'.format('Data;Status;Time;Description'))
    for lane in data:
        file.write('{}\n'.format(lane))
    file.close()
    return

def findDate(output):
    date_positions = []
    date = []
    date_regex = re.compile(r'[A-Za-z]{3,}, [A-Za-z]{3,} {1,2}[0-9]{1,2}, [0-9]{4}')
    for l in range(0, len(output)):
        if re.match(date_regex, output[l]):
            date_positions.append(l)
            date.append(output[l])
    print(date_positions, len(date_positions))
    print(date, len(date))
    #print(len(output))
    #print(output[57239], output[57273])
    return date_positions, date

def convertTable(output, date_positions, date):
    tabela = []
    for r in range(0, len(date_positions)):
        poczatek = date_positions[r]+1
        if r+1 >= len(date_positions):
            koniec = len(output)
        else:
            koniec = date_positions[r+1]
        for i in range(poczatek, koniec, 3):
            status = output[i]
            time = output[i+1]
            desc = output[i+2]
            if re.search(r'HTTP CRITICAL: Status line output matched', desc):
                #print('crit matched')
                continue
            if status == 'NOTIFICATION' and re.search(r'HTTP OK: Status line output matched', desc):
                continue
            if status == 'NOTIFICATION' and re.search(r'This notification was not sent out to any contact', desc):
                #print('kontakt')
                continue
            if status != 'OK' and status != 'NOTIFICATION':
                #print('->', status)
                continue
            else:
                if status == output[i-3] and time == output[i+1-3] and desc == output[i+2-3]:
                    continue
                else:
                    tabela.append(date[r]+';'+status+';'+time+';'+desc)
    return tabela

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    #soup = LoadFile('out.txt', 1)
    #content, label, date, text, output = extractData(soup)
    #print(label, len(label))
    #print(date, len(date))
    #print(content, len(content))
    #saveText(output)
    #print(output)
    output = loadFile('text.txt', 0)
    date_positions, date = findDate(output)
    saveFile(convertTable(output, date_positions, date))



