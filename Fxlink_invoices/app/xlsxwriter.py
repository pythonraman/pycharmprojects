#!/usr/bin/env python

import xlwt
import logging

def write_sheet(sheet,data):
    logging.debug("Writing data to worksheet.")
    for nrow,drow in enumerate(data):
        for ncol,dcol in enumerate(drow):
            sheet.write(nrow,ncol,dcol)
    
def write(path,datarempcs):
    logging.info("Saving output to %s." %path)
    out = xlwt.Workbook()
    logging.debug("Creating worksheet.")
    sheet = out.add_sheet("Remaining")
    write_sheet(sheet,datarempcs)
    logging.debug("Saving worksheet.")
    out.save(path)