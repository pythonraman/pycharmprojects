#!/usr/bin/env python

import os
import re
import sys
import logging

def get_all_sents_for_partno(data,partno,invoice):
    tmp=[]
    for i in range(1,len(data)-1):
        if data[i][1]==partno and data[i][0]==invoice:
            tmp.append(data[i])
    return tmp

def check_if_remaining_and_pcs_are_correct(data,datarempcs):
    logging.info("Checking if pcs and remaining are correct.")
    for i in range(1,len(datarempcs)):
        invoice=datarempcs[i][0]
        partno=datarempcs[i][1]
        pcs=datarempcs[i][2]
        rem=datarempcs[i][3]
        tmp=get_all_sents_for_partno(data,partno,invoice)
        try:
            for y in range(len(tmp)):
                pcs=pcs-tmp[y][2]
        except:
            pcs=-99999
        status=(rem==pcs)
        x=list(datarempcs[i])
        x.extend([str(status)])
        datarempcs[i]=x
    return datarempcs