#!/usr/bin/env python

import xlrd
import os
import logging

def get_ogl_data(oglsheet,descinfo):
    logging.info("Getting OGL info.")
    logging.debug("Desc info: %s" % descinfo)
    data={}

    logging.info("Info fetched. Processing")
    for i in range(oglsheet.nrows-descinfo['row']):
        data[str(oglsheet.col(descinfo['faktura'])[i].value)]=oglsheet.col(descinfo['ogl'])[i].value
    logging.debug("Returning data: %s" % data)
    return data

def get_desc_row_info(oglsheet):
    logging.info("Looking for description row and checking format.")
    ogl={}
    for nrow in range(oglsheet.nrows):
        logging.debug("Processing row %d." % nrow)
        for ncol in range(len(oglsheet.row(nrow))):
            if "faktura" in oglsheet.row(nrow)[ncol].value.lower():
                ogl['faktura']=ncol
            if oglsheet.row(nrow)[ncol].value.lower().startswith("ogl"):
                ogl['ogl']=ncol
            if 'faktura' in ogl and 'ogl' in ogl:
                break;
        if 'faktura' in ogl and 'ogl' in ogl:
            ogl['row']=nrow
            logging.info("All data found.")
            break
        else:
            ogl={}
    return ogl

def read_ogl(path):
    if not os.path.isfile(path):
        logging.warning("OGL file %s doesn't exists. Ignoring" % path)
        return {}
    logging.info("Opening OGL file %s." % path)
    oglfile=xlrd.open_workbook(path)
    for oglsheet in oglfile.sheets():
        logging.info("Processing sheet %s." %  oglsheet.name)
        descinfo=get_desc_row_info(oglsheet)
        if descinfo:
            return get_ogl_data(oglsheet,descinfo)
        else:
            return []

if __name__ == '__main__':
    print("This file is part of FlexLink_invoices application. This can't be run directly.")
    sys.exit(1)