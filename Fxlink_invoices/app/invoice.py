# -*- coding: utf-8 -*-
import os
import re
import sys
import logging
import ntpath
from xlrd import open_workbook, cellname

def process_data_from_invoice(sheet,desc_row,path,ogl):
    datarempcs=[]
    prev_date=None
    qty=None
    pno=None
    rem=None
    pcs=None
    dnote = None
    rempcsnotdone=True
    filename=ntpath.basename(path)[:-5].replace('.','').replace('-','')
    for ncol in range(len(sheet.row(desc_row-1))):
        #Looking for Qty and Part no column
        cv=sheet.row(desc_row)[ncol].value.lower()
        if cv.lower()=="quantity" or cv=="qty":
            logging.debug("Qty found on col %d." % ncol)
            qty=ncol
        if cv.lower()=="part number":
            logging.debug("Part number found on col %d." % ncol)
            pno=ncol
        if cv.lower()=="remaining":
            logging.debug("Remaining found on col %d." % ncol)
            rem=ncol
        if cv.lower()=="pcs":
            logging.debug("Pcs found on col %d." % ncol)
            pcs=ncol
        if cv.lower() == "delivery note":
            logging.debug("Delivery Note found on col %d." % ncol)
            dnote = ncol

        #if not qty or not pno or not rem or not pcs:
        #    logging.info("Not found one of data: %s, %s, %s, %s." %(pno, pcs, rem, qty))
        if pcs and rem and rempcsnotdone:
            tmprempcs=zip(
                        [filename]*(sheet.nrows-(desc_row+1)),
                        [x.value for x in sheet.col(pno)[desc_row+1:sheet.nrows]],
                        [x.value for x in sheet.col(pcs)[desc_row+1:sheet.nrows]],
                        [x.value for x in sheet.col(rem)[desc_row+1:sheet.nrows]],
                        [x.value for x in sheet.col(dnote)[desc_row + 1:sheet.nrows]],
                        [path]*(sheet.nrows-(desc_row+1)),
            )
            tmp=[x for x in tmprempcs if x[1] and x[2] and x[3]]
            #tmp=[x for x in tmprempcs if x[1] and x[2]]
            datarempcs.extend(tmp)
            rempcsnotdone=False
    logging.debug("Returning:")
    logging.debug("DataRemPcs: %s" % datarempcs)
    return datarempcs

def get_desc_row_if_invoice_sheet(sheet):
    desc_row=None
    for nrow in range(sheet.nrows):
        logging.debug("Processing row %d." % nrow)
        for ncol in range(len(sheet.row(nrow))):
            #print(sheet.row(nrow)[ncol].value)
            if str(sheet.row(nrow)[ncol].value).lower() == "part number":
                logging.info("Found columns descriptions row at row %d." % nrow);
                desc_row=nrow
                break;
        if desc_row:
            break;
    if not desc_row:
        return None
    for ncol in range(len(sheet.row(desc_row))):
        if sheet.row(desc_row)[ncol].value.lower() == "subassembly" and sheet.row(desc_row-1)[ncol].value.lower().startswith("wysy"):
            return desc_row
    return None

def process_invoice_file(path,ogl):
    datarempcs=[]
    logging.debug("Opening file %s." % path)
    invoice = open_workbook(path)
    if not invoice:
        logging.error("Can't open file %s. Skipping." % path)
        return None
    logging.debug("File opened. Processing")
    invoice_found=False
    for sheet in invoice.sheets():
        logging.debug("Checking if sheet %s is a invoice." % sheet.name)
        descrow=get_desc_row_if_invoice_sheet(sheet)
        if descrow:
            logging.info("Found sheet invoice. Processing.");
            tmprempcs=process_data_from_invoice(sheet,descrow,path,ogl)
            datarempcs.extend(tmprempcs)
            logging.info("Processed.");
            invoice_found=True
            break;
    if not invoice_found:
        logging.warn("<IT WASN'T INVOICE FILE>")
    logging.debug("Returning:")
    logging.debug("DataRemPcs: %s" % datarempcs)
    return (datarempcs)

if __name__ == '__main__':
    print("This file is part of FlexLink_invoices application. This can't be run directly!")
    sys.exit(0)
