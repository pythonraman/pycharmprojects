#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import logging
import argparse

from app import ogl
from app import invoice
from app import xlsxwriter
from app import process

class Args:
    pass

def logger_setup(workdir):
    logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s',
                    filename='%s/remaining_worker.log' % workdir,
                    filemode='w')

    console=logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)

# https://stackoverflow.com/questions/229186/os-walk-without-digging-into-directories-below
def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

## MAIN

parser = argparse.ArgumentParser(description="Process remaining in invoices for FlexLink.")
parser.add_argument('directory',help="Directory with invoices",default=".\data")
parser.add_argument('--ogl',help="File with OGL relations",default="")
parser.add_argument('--maxdepth',help="How depth should be files processed",type=int,default=2)
parser.add_argument('--out',help="File where the output will be written. Aplication will OVERWRITE this file if it exists. The default value is directory \rem.xls",default="")
args = Args()
parser.parse_args(namespace=args)

if not os.path.isdir(args.directory):
    print("Directory %s not exists. Can't continue." % args.directory)
    sys.exit(1)

logger_setup(args.directory)

if not args.out:
    args.out=args.directory+'rem.xls'

tmp=os.path.split(args.out)[0]
if not os.path.isdir(tmp):
    logging.error("Out directory %s not exists. Can't continue." % tmp)
    print("Out directory %s not exists. Can't continue." % tmp)
    sys.exit(1)

if args.ogl:
    ogld=ogl.read_ogl(args.ogl)
else:
    ogld={}
    
logging.debug("Iterating over subdirectories and files.")

#data=[["Invoice","Part no","Qty","Date","OGL","Path"]]
datarempcs=[["Invoice","Part no","Pcs","Remaining","Path","status"]]
for subdir,dirs,files in walklevel(args.directory,args.maxdepth):
    for file in files:
        if 'arch' in subdir.lower():
            continue
        path=os.path.join(subdir,file)
        if not file.endswith(".xls") and not file.endswith(".xlsx") and not file.endswith(".xls"):
            logging.debug("File %s doesn't meet the filename criteria. Skipping." % path)
            continue
        logging.info("Processing file %s." % path)
        #(tmpdata,tmpdatarempcs)=invoice.process_invoice_file(path,ogld)
        tmpdatarempcs=invoice.process_invoice_file(path,ogld)
        #data.extend(tmpdata)
        datarempcs.extend(tmpdatarempcs)
#datarempcs=process.check_if_remaining_and_pcs_are_correct(datarempcs)
#data=sorted(data,key=lambda x: (x[0],x[1]))
xlsxwriter.write(args.out,datarempcs)
